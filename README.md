> # Brandon Henao


## Despliege
El despliegue se hace a traves de [Heroku](https://www.heroku.com/) y contenedores mediante DockerFile, esta es la url base donde podemos consumir nuestro servicio [api](https://desolate-beach-06421.herokuapp.com/).

> **Note:** el servicio esta alojado en el servidor con una cuenta gratuita , por ende puede tener retrasos o incluso perdida de la informacion


### Servicio 
El servicio esta hecho en **nodeJS**, **express** , **TypeScript** principalmente y consta de dons endpoints, a continuacion se mostrara un ejemplo de como consumirla con curl.


![esquema](./diagrama.png)


#### Peticion http con verbo **GET**
Se usa solo de pruebas, con parametro name.


**Peticion:**
```sh
curl --location --request GET 'https://desolate-beach-06421.herokuapp.com/pusher/brandon'
```

**Respuesta:**
```sh
{
    "message": "successfully!",
    "response": "Bienvenido a nuestra prueba brandon"
}
```


#### Peticion http con verbo **POST**
Es la que usa nuestra aplicacion para ser consumida por el cliente hecho en **React**.

**Peticion:**
```sh
curl --location --request POST 'https://desolate-beach-06421.herokuapp.com/pusher' \
--header 'Content-Type: application/json' \
--data-raw '{
        "candidates":[
            {
                "name": "Brandon Henao",
                "rating": 5
            },
            {
                "name": "Josep Pedregol",
                "rating": 3
            }
        ],
        "location":{
            "lat": 3.38343663,
            "lng": -76.55610634
        },
        "date": "2020-08-26T05:30:00.000Z"
        
}'
```
**Respuesta:**
```sh
{
    "error": false,
    "message": "successfully!",
    "response": "Se ha realizado con exito la operacion."
}
```




## Herramientas
- Visual studio code
- Postman


## Dependencias
- Pusher
- Node
- nodemon (cuando se esta en modo desarrollo refresca los cambios realizados al instante, sin necesidad de para el servidor y volver a subirlo)
- [[express-validator](https://express-validator.github.io/docs/custom-error-messages.html)] (validar las entradas de los parametros)
- cors
- link-module-alias, para rutas absolutas
- typescript




## Comandos
- npm init , para iniciar el proyecto node
- npm install , install todas las dependencias
- npm run postinstall , para instalar **link-module-alias**
- npm run build , construye en una carpeta dist de la aplicacion en **.js**, esta es ignorada por el **.gitignore** ,por lo tanto es necesario ejecutar este comando si quieres ejecutar el proyecto con .js
- npm run start , ejecuta la aplicacion con archivos de extension **.js** en una carpeta llamada **/dist** , antes de ejecutar este comando se debe ejecutar **npm run build**
- nodemon server.js  , para poner en funcionamiento el proyecto -> starting `ts-node dist/server.js` es requerido haber jecutado con anteriorida el comando **npm run buid** 
- nodemon src/server.ts  , para poner en funcionamiento el proyecto -> starting `ts-node src/server.ts`



## Docker
- **docker build -t service-event  -f Dockerfile .** -> esto es para construir la imagen del docker
- **docker image tag service-event localhost:5000/service-event**
- **docker push localhost:5000/service-event**

En el servidor
- **docker pull localhost:5000/service-event**
- **docker run --restart=always -p 4000:4000 -d  --name service-event localhost:5000/service-event**


Para este caso lo enviamos a **heroku** : con el siguiebte comando 
- heroku container:push web -a desolate-beach-06421 ( donde **desolate-beach-06421** es el nombre del proyecto en heroku ,esto debemos hacerlo en la carpeta donde esta el **Dockerfile** )
- heroku container:release web -a desolate-beach-06421 



## Configuracion
- en el tsconfig debemos añadir la propiedad **outDir** , esta propiedad es importante ya que nos define la ubicacion de salida de los archivos **.js** tras la traspilacion. en este caso es **/dist**
- actualizar el package.json , en la propiedad **main** por la direccion o ruta principal donde se ejecuta nuestro comando start , para este caso **dist/server.js**
- actualizar el package.json , para poder ejecutar scripts para compilar y transpilar nuestro codigo **TypeScript** , para nuestro caso seria algo como esto.
  

```sh
    "scripts": {
        "dev": "ts-node src/server.ts",
        "start": "ts-node dist/server.js",
        "build": "tsc",
        "test": "echo \"Error: no test specified\" && exit 1",
        "postinstall": "link-module-alias"
    },
```