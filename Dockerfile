# base image
FROM node:12.2.0-alpine


# Add Maintainer Info
LABEL maintainer="jbhenao17@misena.edu.co"

WORKDIR /app

# copy configs to /app folder
COPY package.json ./
COPY tsconfig.json ./
# copy source code to /app/src folder
COPY src /app/src


#RUN npm
RUN npm i -g typescript ts-node

COPY . ./
# TypeScript
RUN npm install && npm run postinstall 

# start app
CMD ["npm", "run","dev"]

